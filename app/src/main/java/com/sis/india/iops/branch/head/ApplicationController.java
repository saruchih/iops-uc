package com.sis.india.iops.branch.head;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.sis.india.iops.branch.head.deps.AppComponent;
import com.sis.india.iops.branch.head.deps.AppModule;
import com.sis.india.iops.branch.head.deps.DaggerAppComponent;
import com.sis.india.iops.branch.head.deps.RoomModule;
import com.sis.india.iops.branch.head.networking.NetworkModule;
import com.sis.india.iops.branch.head.repository.LocalRepositoryImpl;
import com.sis.india.iops.branch.head.repository.RemoteRepositoryImpl;

import java.io.File;

import io.fabric.sdk.android.Fabric;

public class ApplicationController extends Application
{
    private static AppComponent appComponent;
    private static ApplicationController instance;



    public static ApplicationController get(Context context)
    {
        return (ApplicationController) context.getApplicationContext();
    }
    public static ApplicationController getInstance()
    {
        return instance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .build());

        initAppComponent();
        appComponent.inject(this);

    }

    public AppComponent initAppComponent()
    {
        File cacheFile = new File(getCacheDir(), "responses");

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .roomModule(new RoomModule(this))
                .networkModule(new NetworkModule(cacheFile))

                .build();

        return appComponent;
    }

    public static AppComponent getAppComponent()
    {
        return appComponent;
    }


    public LocalRepositoryImpl localRepoInstance(){
        return appComponent.getLocalRepo();
    }
    public RemoteRepositoryImpl remoteRepositoryInstance(){
        return appComponent.getRemoteRepo();
    }


}

