package com.sis.india.iops.branch.head.base;

import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.sis.india.iops.branch.head.base.listner.ActionBarView;
import com.sis.india.iops.branch.head.networking.NetworkChangeReceiver;
import com.sis.india.iops.branch.head.util.AppPreferences;
import com.sis.india.iops.branch.head.util.ObjectUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;


import static android.text.TextUtils.isEmpty;
import static com.sis.india.iops.branch.head.util.ObjectUtil.isNull;

/**
 * saruchi
 */
public abstract class BaseActivity extends AppCompatActivity implements ActionBarView {
    public NetworkChangeReceiver networkCheck;
    public BasePresenter presenter;
    private Unbinder unbinder;
    protected Resources mResources;
    protected AppPreferences appPreferences;
    protected abstract void initializeDagger();
    protected abstract void initializePresenter();
    public abstract int getLayoutId();
    protected abstract void setUpUi();
    ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
        mResources= getResources();
        appPreferences= new AppPreferences(this);
        initializeDagger();
        initializePresenter();
        if (presenter != null) {
            presenter.initialize(getIntent().getExtras());
        }
        setUpUi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (presenter != null) {
            presenter.start();
        }
    }

    @Override
    public void setUpIconVisibility(boolean visible) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(visible);
        }
    }

    @Override
    public void setToolbarTitle(String titleKey) {
        final ActionBar actionBar = getSupportActionBar();
        if (!isNull(actionBar)) {
            if (!isEmpty(titleKey)) {
                actionBar.setTitle(titleKey);
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
       setNetworkCheck();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (!isNull(networkCheck ))
                unregisterReceiver(networkCheck);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.finalizeView();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    /**\
     * for network connection detection
     */
    private void setNetworkCheck(){
        try {
            networkCheck = new NetworkChangeReceiver(this);

            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            registerReceiver(networkCheck, filter);

        } catch (Exception e) {
           // finish();
        }
    }


}
