package com.sis.india.iops.branch.head.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public abstract class BaseFrame extends FrameLayout {

    public BaseFrame(Context context) {
        super(context);
    }

    public BaseFrame(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public void show(View... views) {
        for (View view : views) {
            view.setVisibility(VISIBLE);
        }
    }

    public void hideGone(View... views) {
        for (View view : views) {
            view.setVisibility(GONE);
        }
    }


    public void hideInvisible(View... views) {
        for (View view : views) {
            view.setVisibility(INVISIBLE);
        }
    }
}