package com.sis.india.iops.branch.head.base.listner;


import com.sis.india.iops.branch.head.networking.NetworkError;

import retrofit2.Response;

/**
 * Created by saruchiarora on 1/20/18.
 */
public interface BaseCallback {
    void onSuccess(Response response);
    void onFail(NetworkError networkError);
}