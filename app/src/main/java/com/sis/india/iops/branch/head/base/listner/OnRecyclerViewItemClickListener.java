package com.sis.india.iops.branch.head.base.listner;

import android.view.View;

public interface OnRecyclerViewItemClickListener {

    void onRecyclerViewItemClick(View v, int position);
}