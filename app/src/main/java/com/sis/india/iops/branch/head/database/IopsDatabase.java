package com.sis.india.iops.branch.head.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.sis.india.iops.branch.head.util.Converters;

import static com.sis.india.iops.branch.head.database.IopsDatabase.VERSION;

@Database(entities = {},version = VERSION)
@TypeConverters({Converters.class})  //convert ArrayList object to one of SQLite data type
public abstract class IopsDatabase extends RoomDatabase {
    static final int VERSION = 1;
    public abstract IopsDao getIopsDao();
}