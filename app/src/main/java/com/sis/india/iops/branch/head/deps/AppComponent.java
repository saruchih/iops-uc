
package com.sis.india.iops.branch.head.deps;

import android.app.Application;

import com.sis.india.iops.branch.head.ApplicationController;
import com.sis.india.iops.branch.head.database.IopsDao;
import com.sis.india.iops.branch.head.database.IopsDatabase;
import com.sis.india.iops.branch.head.networking.NetworkModule;
import com.sis.india.iops.branch.head.repository.LocalRepositoryImpl;
import com.sis.india.iops.branch.head.repository.RemoteRepositoryImpl;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(dependencies = {}, modules = {AppModule.class, RoomModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(ApplicationController applicationController);



    IopsDao iopsDao();
    IopsDatabase iopsDatabase();
    RemoteRepositoryImpl getRemoteRepo();
    LocalRepositoryImpl getLocalRepo();
    Application application();
    BaseUrlHolder provideBaseUrlHolder();
    Retrofit getRetrofit();
}