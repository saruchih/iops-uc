package com.sis.india.iops.branch.head.deps;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.sis.india.iops.branch.head.database.IopsDao;
import com.sis.india.iops.branch.head.database.IopsDatabase;
import com.sis.india.iops.branch.head.networking.NetworkService;
import com.sis.india.iops.branch.head.repository.LocalRepository;
import com.sis.india.iops.branch.head.repository.LocalRepositoryImpl;
import com.sis.india.iops.branch.head.repository.RemoteRepository;
import com.sis.india.iops.branch.head.repository.RemoteRepositoryImpl;
import com.sis.india.iops.branch.head.util.AppConstants;

import java.util.concurrent.Executor;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;


@Module
public class RoomModule {

    private IopsDatabase iopsDatabase;

    public RoomModule(Application mApplication) {
        iopsDatabase = Room.databaseBuilder(mApplication, IopsDatabase.class, AppConstants.DATABASE_NAME).build();
    }

    @Singleton
    @Provides
    IopsDatabase providesRoomDatabase() {
        return iopsDatabase;
    }

    @Singleton
    @Provides
    IopsDao providesIglDao(IopsDatabase iopsDatabase) {
        return iopsDatabase.getIopsDao();
    }

    @Singleton
    @Provides
    public RemoteRepository getRemoteRepo(NetworkService networkService){
        return new RemoteRepositoryImpl(networkService);
    }
    @Singleton
    @Provides
    public LocalRepository getLocalRepo(IopsDao iopsDao, Executor exec){
        return new LocalRepositoryImpl(iopsDao, exec);
    }
    @Provides @Named("activity")
    public CompositeDisposable getCompositeDisposable(){
        return new CompositeDisposable();
    }

    @Provides @Named("vm")
    public CompositeDisposable getVMCompositeDisposable(){
        return new CompositeDisposable();
    }

}