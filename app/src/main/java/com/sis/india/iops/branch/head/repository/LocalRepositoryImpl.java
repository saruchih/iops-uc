package com.sis.india.iops.branch.head.repository;


import com.sis.india.iops.branch.head.database.IopsDao;

import java.util.concurrent.Executor;

import javax.inject.Inject;

public class LocalRepositoryImpl implements LocalRepository {
    private IopsDao iopsDao;
    private Executor executor;

    @Inject
    public LocalRepositoryImpl(IopsDao cDAO, Executor exec) {
        iopsDao = cDAO;
        executor = exec;
    }
}
