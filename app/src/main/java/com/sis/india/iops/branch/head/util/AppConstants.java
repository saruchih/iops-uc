package com.sis.india.iops.branch.head.util;

public interface AppConstants {
    String DATABASE_NAME = "iops_branchhead.db";

    // ---------- Network Connection -----------------
    String NOINTERNET = "NOINTERNET";
    String INTERNETENABLED = "INTERNET ENABLED";
    int CODE_200=200;
    int CODE_202=202;
    int CODE_404=404;
    int CODE_417=417;
}
