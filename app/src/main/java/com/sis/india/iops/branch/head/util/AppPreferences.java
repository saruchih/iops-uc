package com.sis.india.iops.branch.head.util;

import android.content.Context;
import android.content.SharedPreferences;


public class AppPreferences {

    private static final String KEY_LOGIN_STATUS	= "loginstatus";


    private static final String APP_SHARED_PREFS	= AppPreferences.class.getSimpleName();
    private final Context mcontext;
    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    public AppPreferences(Context context) {
        this.sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
        this.prefsEditor = sharedPrefs.edit();
        this.mcontext = context;
    }
    public void clearall() {
        prefsEditor.clear();
        prefsEditor.commit();
    }
    public boolean getLoginStatus() {
        return sharedPrefs.getBoolean(KEY_LOGIN_STATUS,false);
    }

    public void saveLoginStatus(boolean isLogin) {
        prefsEditor.putBoolean(KEY_LOGIN_STATUS, isLogin);
        prefsEditor.commit();
    }
}
