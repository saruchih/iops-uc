

package com.sis.india.iops.branch.head.util;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 *  @TypeConverters annotation we provided along with @Database annotation, this will be the class
 *  which will tell Room how to convert ArrayList object to one of SQLite data type.
 *  We will implement methods to convert ArrayList to String for storing it in DB
 *  and String back to ArrayList for getting back original User object.
 */
public class Converters {
    @TypeConverter
    public static ArrayList<String> fromTimestamp(String value) {
        Type listType = new TypeToken<ArrayList<String>>() {}.getType();
        return new Gson().fromJson(value, listType);
       // return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static String arraylistToString(ArrayList<String> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        return json;
       // return date == null ? null : date.getTime();
    }
}