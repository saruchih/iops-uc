package com.sis.india.iops.branch.head.util;

import android.util.Log;

public class Logger {


    public Logger() {
    }

    public static void d(String tag , String message){
        Log.d(tag,message);
    }

    public static void e(String tag , String message){
        Log.e(tag,message);
    }
}
