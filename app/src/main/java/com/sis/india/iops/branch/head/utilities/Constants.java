package com.sis.india.iops.branch.head.utilities;

/**
 * Created by Ashu Rajput on 5/14/2018.
 */

public interface Constants {

    // The account name : If changed, change in stings.xml also
    String ACCOUNT = "IOPSBRANCHHEAD";

    // An account type, in the form of a domain name: If changed, change in stings.xml also
    String ACCOUNT_TYPE = "com.sis.india.iops.branch.head";

    // if changed, change in stings.xml also
    String AUTHORITY = "com.sis.india.iops.branch.head.android.syncadpter.provider";
}
